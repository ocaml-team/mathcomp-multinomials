mathcomp-multinomials (2.2.0-4) unstable; urgency=medium

  * Bump standards-version to 4.7.0.
  * Add patch to compile with newer coq-elpi.

 -- Julien Puydt <jpuydt@debian.org>  Wed, 20 Nov 2024 10:59:49 +0100

mathcomp-multinomials (2.2.0-3) unstable; urgency=medium

  * Team upload
  * Do not hardcode /usr/lib/ocaml

 -- Stéphane Glondu <glondu@debian.org>  Sat, 03 Aug 2024 14:34:37 +0200

mathcomp-multinomials (2.2.0-2) unstable; urgency=medium

  * Bump for binary compatibility.

 -- Julien Puydt <jpuydt@debian.org>  Sat, 30 Mar 2024 11:51:17 +0100

mathcomp-multinomials (2.2.0-1) unstable; urgency=medium

  * New upstream release.

 -- Julien Puydt <jpuydt@debian.org>  Sat, 20 Jan 2024 20:33:52 +0100

mathcomp-multinomials (2.1.0-1) unstable; urgency=medium

  * Fix d/rules clean target (Closes: #1048599).
  * New upstream release.

 -- Julien Puydt <jpuydt@debian.org>  Fri, 22 Dec 2023 11:35:03 +0100

mathcomp-multinomials (1.6.0-3) unstable; urgency=medium

  * Fix compilation with recent dune.

 -- Julien Puydt <jpuydt@debian.org>  Thu, 06 Jul 2023 11:51:16 +0200

mathcomp-multinomials (1.6.0-2) unstable; urgency=medium

  * Fix autopkgtest (Closes: #1038695).

 -- Julien Puydt <jpuydt@debian.org>  Tue, 20 Jun 2023 11:39:20 +0200

mathcomp-multinomials (1.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards-version to 4.6.2.

 -- Julien Puydt <jpuydt@debian.org>  Thu, 15 Jun 2023 07:22:40 +0200

mathcomp-multinomials (1.5.5-8) unstable; urgency=medium

  * Add an autopkgtest.

 -- Julien Puydt <jpuydt@debian.org>  Tue, 05 Jul 2022 09:57:33 +0200

mathcomp-multinomials (1.5.5-7) unstable; urgency=medium

  * Fix use of dh-coq.
  * Fix installation directory.

 -- Julien Puydt <jpuydt@debian.org>  Tue, 28 Jun 2022 08:46:47 +0200

mathcomp-multinomials (1.5.5-6) unstable; urgency=medium

  * Use dh-coq.

 -- Julien Puydt <jpuydt@debian.org>  Sun, 12 Jun 2022 14:25:27 +0200

mathcomp-multinomials (1.5.5-5) unstable; urgency=medium

  * Re-upload because of ABI breakage.

 -- Julien Puydt <jpuydt@debian.org>  Thu, 02 Jun 2022 13:44:54 +0200

mathcomp-multinomials (1.5.5-4) unstable; urgency=medium

  * Re-upload because of ABI breakage.

 -- Julien Puydt <jpuydt@debian.org>  Tue, 24 May 2022 09:08:01 +0200

mathcomp-multinomials (1.5.5-3) unstable; urgency=medium

  * Complete d/copyright.
  * Bump standards-version to 4.6.1.

 -- Julien Puydt <jpuydt@debian.org>  Tue, 17 May 2022 22:02:41 +0200

mathcomp-multinomials (1.5.5-2) unstable; urgency=medium

  * Fix dep on the right coq ABI.

 -- Julien Puydt <jpuydt@debian.org>  Thu, 05 May 2022 22:26:02 +0200

mathcomp-multinomials (1.5.5-1) unstable; urgency=medium

  * Initial release. (Closes: #1010637)

 -- Julien Puydt <jpuydt@debian.org>  Thu, 05 May 2022 22:15:32 +0200
